const body = document.querySelector('body');
const table = document.createElement('table');
body.prepend(table);
for (let i = 0; i < 30; i++) {
    const tr = document.createElement('tr');
    table.append(tr);
    for (let i = 0; i < 30; i++) {
        const td = document.createElement('td');
        tr.append(td);
    };
};

table.addEventListener('click', function (event) {
    const square = event.target;
    square.classList.toggle("black");
});

body.addEventListener('click', function (event) {
    if (event.target.tagName != 'TD') {
        const squares = document.querySelectorAll('td');
        squares.forEach(function (element) {
            if (element.classList.contains("black")) {
                element.classList.remove("black");
            } else {
                element.classList.add("black");
            };
        });
    };
});